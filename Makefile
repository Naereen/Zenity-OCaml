# Module Zenity pour Ocaml, http://besson.qc.to/publis/Zenity/
# (C) 2013-2016, Lilian Besson
# GPLv3 Licensed, see http://besson.qc.to/LICENSE

all: zenity.cma zenity.cmxa doc clean

libs:	zenity.cma zenity.cmxa

zenity.cma:
	@echo " Construction de la bibliothèque zenity.cma (ocamlc)"
	ocamlc -a -o zenity.cma graphics.cma zenity.mli zenity.ml

zenity.cmxa:
	@echo " Construction de la bibliothèque zenity.cmxa (ocamlopt)"
	ocamlopt -a -o zenity.cmxa zenity.mli zenity.ml

doc: readme doctex dochtml

readme:
	rst2html README.rst > index.html

doctex:
	@echo " Génération de la documentation TEX ..."
	ocamldoc -charset utf-8 -latex -o zenity.tex zenity.mli
	pdflatex zenity.tex
	pdflatex zenity.tex

dochtml:
	@echo " Génération de la documentation HTML ..."
	mkdir --parents --verbose doc
	ocamldoc -charset utf-8 -html -d doc -o zenity zenity.mli

help:
	@echo " RTFM"

clean:
	@echo " Cleaning useless files..."
	-rm -vf *.a *.o *.~
	-rm -rvf zenity.tex *.aux *.synctex* *.toc ocamldoc.sty *.log

cleanall: clean
	@echo " Cleaning all files..."
	-rm -vf *.cma *.cmo *.cmi *.cmx *.cmxa index.html
	-rm -rvf zenity.pdf doc/
