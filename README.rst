Une interface fonctionnelle à Zenity, pour Ocaml (v3.11+)
#########################################################
  `Désormais sur Bitbucket ! <https://bitbucket.org/lbesson/zenity-ocaml/>`_


.. warning:: *Mais euh... à quoi ça sert ?*

Ce petit projet est une bibliothèque ("library") `OCaml <http://ocaml.org/>`_ qui permet d'utiliser l'outil `Zenity <https://wiki.gnome.org/Projects/Zenity>`_ depuis un terminal ou un programme écrit en OCaml.

Zenity permet d'afficher des fenêtres interactives dans des scripts bash (ou shell), comme par exemple : ::

    $ zenity --title "Mon titre" --info --text "Voici un exemple."


Et ma petite bibliothèque permet de faire la même chose depuis OCaml (soit un terminal interactif, soit un programme interprété ou compilé, avec ``ocamlc`` ou ``ocamlopt``) : ::

    $ ocaml zenity.cma

        OCaml version 4.03.0

    # open Zenity;;
    # info ~title:"Mon titre" ~text:"Voici un exemple." ();;
    - : unit = ()


Les deux appels auront le même effet, à savoir afficher cette fenêtre :

.. image:: fenetre_info.png


L'avantage de cette bibliothèque est de proposer une interface OCaml propre à Zenity, et surtout, une interface *bien typée* !
Par exemple, voici la signature de cette fonction ``Zenity.info`` : ::

    # info;;
    - : ?title:string ->
    ?window_icon:string ->
    ?width:int ->
    ?height:int ->
    ?expiration:int ->
    ?text:string ->
    ?no_wrap:bool ->
    unit -> unit
    = <fun>

Elle peut donc être appelée ``info ();;`` pour utiliser les arguments par défaut,
ou pour un exemple plus complet : ::

    # info ~width:880 ~height:480
    ~expiration:30 ~no_wrap:false
    ~window_icon:"/usr/share/icons/hicolor/48x48/apps/vim.png"
    ~title:"Vim Notification"
    ~text:"Some text ... sent from OCaml"


Chaque argument optionnel est décrit avec détail dans `la documentation <doc/Zenity.html>`_.
Il y a de nombreuses autres fonctions.
Pour plus de détails, veuillez lire la documentation de zenity (``zenity --help-all``).

----

Dépendances
^^^^^^^^^^^
- `Zenity <https://wiki.gnome.org/Projects/Zenity>`_ (normalement déjà installé sous Linux),
- `OCaml <http://ocaml.org/>`_, version v3.11+ (testé avec la dernière version, v4.03.0, 25-04-2016),
- `GNU Make <https://en.wikipedia.org/wiki/GNU_make>`_ (normalement déjà installé sous Linux et Mac OS X).

Installation
^^^^^^^^^^^^
 #. Télécharger l'archive zip : http://besson.qc.to/publis/Zenity.zip (aussi `ici <https://bitbucket.org/lbesson/zenity-ocaml/get/master.zip>`_),
 #. Décompresser, allez dans le dossier,
 #. Faire "make",
 #. Copier les deux librairies ``zenity.cma`` et ``zenity.cmxa``, dans le dossier de votre projet,
 #. Compiler votre projet avec "``ocamlc ... zenity.cma ...``"  ou "``ocamlopt ... zenity.cmxa ...``" (i.e. charger les bibliothèques à la compilation),
 #. Dans les sources, ouvrir le module avec "open Zenity;;", et utiliser les fonctions `décrites dans cette documentation <doc/Zenity.html>`_.
 #. La documentation est aussi disponible `en PDF <zenity.pdf>`_, si besoin.

----

Sources
^^^^^^^
Les trois fichiers importants sont

 * `<zenity.ml>`_ (code),
 * `<zenity.mli>`_ (interface),
 * `<Makefile>`_ (script de compilation).

Exemple
^^^^^^^
Le code OCaml suivant ::

    open Zenity;;
    info ~title:"Titre vide" ~expiration:5 ();;

ouvre une fenêtre avec le titre "Titre vide", le message par défaut ("Toutes les mises-à-jour sont complètes"), et une durée de vie de 5 secondes.

.. image:: demo.png

----

Licence
^^^^^^^
Distribué sous `licence MIT <http://lbesson.mit-license.org/>`_.

Auteur ?
^^^^^^^^
Ce projet est (C) 2012-2016 `Lilian Besson <http://perso.crans.org/besson/>`_,
et `sur bitbucket <https://bitbucket.org/lbesson/>`_.
