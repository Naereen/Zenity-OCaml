(** Une interface fonctionnelle à l'outil Zenity
    Dernière version ici: http://besson.qc.to/publis/Zenity/
    @author Lilian Besson <lilian.besson[AT]ens-cachan.fr>
*)

exception ZenityOcaml_Erreur_Annule of int
(** L'exception qui est renvoyé si le processus appelé par les
    fonctions suivantes retourne un code d'erreur UNIX spécifiant une
    erreur. *)

val ecrire_dans_fichier : chaine:string -> adresse:string -> unit
(** Ecrit la chaine [chaine] dans le fichier contenu à l'adresse
    relative ou absolue [adresse]. Lève une exception si ce fichier
    est absent. *)

val nom_fichier_temporaire : string
(** Nom du fichier temporaire ou est stocké le résultat de l'appel
    à Zenity. *)

val fairezenity : string -> unit
(** Appel zenity avec une commande, et fork le résultat dans
    nom_fichier_temporaire*)

(** {6 Définition des différentes fonctions utiles du module ZenityOcaml } *)
(** Toutes ces fonctions ont des paramètres uniquement optionnels,
    qui sont initialisés avec les valeurs par défaut.  Chaque
    fonction a au moins les paramètres communs [~title],
    [~window_icon], [~width], [~height], [~expiration]. *)


(** {7 zenity --calendar} *)

val calendar :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?text:string ->
  ?day:int ->
  ?month:int -> ?year:int -> ?date_format:string -> unit -> string
(** @param title Fixe le titre de la fenètre GTK+ créée par la
    fonction.  @param window_icon Adresse relative ou absolue où
    aller chercher une image d'icone pour la fenètre (en béta).
    @param width Largeur de la fenètre. En dehors d'une certaine
    plage de valeurs, n'a plus d'effets.  @param expiration Fixe
    un temps en secondes avant la fermeture automatique de la
    fenètre. Pas encore supporté pour toutes les fonctions (en
    béta).  @param text Fixe le message qui s'affiche dans la
    boite de dialogue.  @param day Numéro du jour
    pré-sélectionné dans la boite de dialogue (entre 1 et 31).
    @param month Idem pour le mois (entre 1 et 12).  @param year
    Idem pour l'année.  @param date_format Not for casual
    users. *)
(** Ouvre une demande de sélection de date. *)

(** {7 zenity --entry} *)

val entry :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?text:string -> ?entry_text:string -> ?hide_text:bool -> unit -> string
(** @param entry_text Fixe le texte déjà écrit par défaut dans la
    boite de dialogue.  @param hide_text Si présent et vrai (=
    [true]) le texte est caché lors de la frappe. *)
(** Ouvre une demande de texte. *)


(** {7 zenity --password} *)

val password :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int -> ?expiration:int -> ?username:string -> unit -> string
(** @param username Nom de l'utilisateur auquel on demande le mot de
    passe (facultatif). *)
(** Ouvre une demande de mot de passe. *)


type add_forms =
    Add_entry of string
  (** Ajoute une entrée requête de texte. *)
  | Add_password of string
  (** Ajoute une entrée requête de mot de passe. *)
  | Add_calendar of string
  (** Ajoute une entrée requête de date. *)
  | NoAdd
(** N'ajoute rien. *)
(** Type des entrées supplémentaires de la fonction forms
    (formulaire). *)

val print_add_forms : add_forms -> string
(** Une méthode simple pour afficher des éléments de ce type. *)

(** {7 zenity --forms} *)

val forms :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?text:string ->
  ?separator:string ->
  ?forms_date_format:string -> ?add:add_forms list -> unit -> string
(** @param add Liste des éléments ajoutés au formulaire. L'ordre
    compte, et la liste peut etre nulle.  @param separator Inutile
    [pas visible]. *)
(** Ouvre un formulaire. Voir [add_forms] pour plus de détails. *)


(** {7 zenity --color-selection} *)

val color_selection :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?color:Graphics.color -> unit -> Graphics.color
(** @param color Fixe la couleur choisie par défaut pour la fenètre
    de demande de sélection de couleur. *)
(** Ouvre une demande de sélection de couleur. Nécessite le module
    Graphics, inclus dans les versions compilées zenity.cma et
    zenity.cmxa. Il n'y a normalement rien d'autre à faire pour
    l'utilisation de ZenityOcaml. *)

(** {7 zenity --scale} *)

val scale :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?text:string ->
  ?value:int ->
  ?min_value:int ->
  ?max_value:int ->
  ?step:int -> ?print_partial:bool -> ?hide_value:bool -> unit -> int
(** @param value Fixe la valeur choisie par défaut pour la fenètre
    de demande de choix de valeur.  @param min_value Fixe la
    valeur minimale qu'il sera possible de choisir.  @param
    max_value Fixe la valeur maximale qu'il sera possible de
    choisir.  @param step Fixe le pas de l'interval dans lequel il
    sera possible de choisir.  @param hide_value Si présent et
    vrai (= [true]) la valeur courante est cachée lors de la
    frappe.  @param print_partial Undocumented. *)
(** Ouvre une demande de valeurs entières. *)


(** {7 zenity --file-selection} *)

val file_selection :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?filename:string ->
  ?multiple:bool ->
  ?directory:bool ->
  ?save:bool ->
  ?separator:string ->
  ?confirm_overwrite:bool -> ?file_filter:string -> unit -> string list
(** @param filename Nom du fichier surlequel la sélection est faite
    par défaut.  @param multiple Si présent et vrai (= [true])
    la sélection autorise plusieurs fichiers.  @param directory
    Si présent et vrai (= [true]) la sélection n'autorise que la
    sélection des dossiers.  @param file_filter Expression
    régulière bash faisant office de filtre sur les fichiers
    sélectionnables. *)
(** Ouvre une demande de sélection de fichier dans un explorateur de dossiers. *)


(** {7 zenity --list} *)

val list :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?text:string ->
  ?column:string ->
  ?checklist:bool ->
  ?radiolist:bool ->
  ?separator:string ->
  ?multiple:bool ->
  ?editable:bool ->
  ?print_column:int ->
  ?hide_column:int ->
  ?hide_header:bool -> ?liste:string list -> unit -> string list
(** @param column Item inscrit dans la colonne.  @param liste Liste,
    vide ou non, des éléments dans la colonne.  @param multiple Si
    présent et vrai (= [true]) la sélection autorise plusieurs
    choix.  @param checklist Si présent et vrai (= [true]) la
    sélection autorise plusieurs choix en cochant. *)
(** Ouvre une demande de sélection dans une liste construite par
    l'utilisateur (argument liste). Le paramètre separator est
    inutile. Les paramètres non documentés ne sont pas encore bien
    supportés (béta). *) (* TODO : fix it *)


(** {7 zenity --text-info} *)

val text_info :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?filename:string ->
  ?editable:bool ->
  ?display:int ->
  ?html:bool ->
  ?url:string -> ?checkbox:string -> ?font:string -> unit -> string
(** @param display Précise le serveur X a utiliser pour afficher la
    fenètre GTK.  @param editable Permet de rendre le contenu de
    la fenètre éditable.  @param checkbox Fixe le message de la
    boite de confirmation.  @param filename Renseigne l'adresse
    relative ou absolue du fichier a afficher.  @param font
    Renseigne la police à utiliser pour afficher le contenu de la
    boite. Une liste complète des polices supportées devrait
    voir le jour bientôt.  @param html Doit etre actif pour
    utiliser url, qui affiche le contenu d'une page web (ou d'un
    document en renseignant son adresse URI).  @param url
    Renseigne l'adresse de la page web a afficher.  *)
(** Ouvre une fenètre informative contenant du texte. *)


(** {7 Edit} *)

val edit :
  string ->
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?display:int -> ?font:string -> unit -> string
(** Fournit un éditeur de texte [minimal].  Le premier paramètre
    sans nom correspond à l'adresse relative ou absolue du fichier à
    éditer. Bien sur, les fonctionnalités d'un tel éditeur sont
    très limitées. *)

(** {7 zenity --error} *)

val error :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?text:string -> ?no_wrap:bool -> unit -> unit
(** Ouvre une fenètre d'erreur. Lève une exception si l'utilisateur
    n'appuie pas sur le bouton 'positif'. *)


(** {7 zenity --info} *)

val info :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?text:string -> ?no_wrap:bool -> unit -> unit
(** Ouvre une fenètre informative. Lève une exception si
    l'utilisateur n'appuie pas sur le bouton 'positif'. *)


(** {7 zenity --question} *)

val question :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int ->
  ?text:string ->
  ?ok_label:string -> ?cancel_label:string -> ?no_wrap:bool -> unit -> unit
(** @param ok_label Fixe le contenu du message de la boite 'positive'
    de confirmation.  @param cancel_label Fixe le contenu du
    message de la boite 'négative' d'annulation.  *)
(** Ouvre une demande de réponse à une question. Lève une exception
    si l'utilisateur n'appuie pas sur le bouton 'positif'. *)


(** {7 zenity --warning} *)

val warning :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?text:string -> ?no_wrap:bool -> unit -> unit
(** Ouvre une fenètre d'avertissement. Lève une exception si
    l'utilisateur n'appuie pas sur le bouton 'positif'.  *)


(** {7 zenity --notification} *)

val notification :
  ?title:string ->
  ?window_icon:string ->
  ?width:int ->
  ?height:int ->
  ?expiration:int -> ?text:string -> ?listen:bool -> unit -> int
(** @param listen Not for casual users. *)
(** Ouvre une fenètre d'avertissement. Ne fait rien si text est
    vide. *)


type urgency_level = Low | Normal | Critical
(** Réprésente les trois niveaux d'urgence utilisées pour la
    fonction notify. *)
val print_urgency_level : urgency_level -> string

(** {7 notify} *)

val notify :
  ?urgency:urgency_level ->
  ?expiration:int ->
  ?icon:string -> ?body:string -> ?summary:string -> unit -> unit
(** @param urgency Niveau d'urgence de la notification.  @param body
    Contenu du message de la notification.  @param summary Titre
    de la notification. *)
(** Affiche une notification système, dans la zone prévue à cet
    effet. summary doit etre non vide. *)


val help : string -> string
(** Affiche l'aide de zenity concernant le topic argument. *)

